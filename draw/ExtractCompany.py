import copy


class ExtractObject(object):
    def __init__(self, name):
        self.extact_name = name
        self.company_list = []
        self.company_num = 0

    def addcompany(self, name):
        if 0 != len(name):
            self.company_num += 1

    def delcompany(self, name):
        if 0 != len(name):
            if name in self.company_list:
                #self.company_list.remove(name)
                self.company_num -= 1

    def GetListName(self):
        return self.extact_name

    def GetCompanyList(self):
        return self.company_list

    def GetCompanyNum(self):
        return len(self.company_list)

    def SetListName(self, name):
        if 0 != len(name):
            self.extact_name = name

    def SetCompanyList(self, list):
        self.company_list = copy.deepcopy(list)
