from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *
from setttingDialog import Ui_dialog
from SaveExtractList import SaveExtraListInFile
from ExtractCompany import ExtractObject
import copy
from PyQt5.QtCore import *


def display_extractlist(self):
    listModel = QStringListModel()  # 显示保存好的抽取列表
    listModel.setStringList(self.display_extract_list)
    self.ui.extractList.setModel(listModel)


def display_companylist(self):
    listModel = QStringListModel()  # 显示添加的公司列表
    listModel.setStringList(self.display_company_list)
    self.ui.companyList.setModel(listModel)
    print(self.display_company_list)


class ExtractSettingUi(QWidget):  # 建立第二个窗口的类
    def __init__(self, father_ui):
        QtWidgets.QDialog.__init__(self)
        self.ui = Ui_dialog()
        self.ui.setupUi(self)
        self.father_ui = father_ui   # 获取主窗口的类

        self.ui.SaveButton.clicked.connect(self.save_setting_function)  # 保存按键事件
        self.ui.BackButton.clicked.connect(self.exit_function)          # 返回按键事件
        self.ui.putBackcomboBox.currentIndexChanged.connect(self.putback_index_change)  # 下拉框回调事件
        self.ui.putBackcomboBox.addItems(["可放回", "不可放回"])

        self.ui.AddDrawButton.clicked.connect(self.add_extract_list_button)  # 增加抽取列表按键
        self.ui.AddCompany.clicked.connect(self.add_company_button)          # 增加公司按键

        self.ui.companyList.clicked.connect(self.company_list_clicked)               # 公司列表被选中回调
        # self.ui.companyList.setSelectionMode(QAbstractItemView.ExtendedSelection)  # 公司列表允许多选
        # self.ui.companyList.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.ui.startNum_input.setText("0")
        self.ui.endNum_input.setText("10")

        self.ui.extractList.clicked.connect(self.extract_qlist_select)        # 抽取列表被选中回调
        self.ui.numradioButton.setChecked(True)
        self.ui.inputradioButton.setChecked(False)

        self.which_list_click = 0
        self.extract_list_index = 0  # 当前抽取列表索引
        self.extract_save_list = []  # 从JSON文件读出来的抽取列
        self.display_extract_list = []  # 界面显示的抽取列表
        self.display_company_list = []  # 显示用公司列表
        self.no_put_back_flag = 0    # 是否放回抽取的标志
        self.draw_number_list = list(range(int(self.ui.startNum_input.text()), int(self.ui.endNum_input.text())))  # 抽奖数字范围

        self.save_extract = SaveExtraListInFile("json.txt")
        self.extract_save_list = self.save_extract.get_extral_list()  # 从JSON文件中获取所有抽取列表
        if 0 != len(self.extract_save_list):
            self.display_company_list = self.extract_save_list[0].company_list
            for obj in self.extract_save_list:
                self.display_extract_list.append(obj.GetListName())
            display_companylist(self)
            display_extractlist(self)

    def add_extract_list_button(self):
        list_name = self.ui.list_name.text()
        if list_name == "":
            QMessageBox.information(self, "提示", "列表名无效")
            return
        extract_obj = ExtractObject(list_name)     # 新建一个抽取对象
        self.extract_save_list.append(extract_obj)  # 加入到保存用的列表当中
        self.display_extract_list.append(list_name)  # 加入显示用的抽取列表
        display_extractlist(self)

    def add_company_button(self):
        if 0 == len(self.ui.companyName.text()):
            QMessageBox.information(self, "提示", "公司名无效")
            return
        if 0 == len(self.ui.cur_list_name.text()):
            QMessageBox.information(self, "提示", "请选中列表")
            return
        company_name = self.ui.companyName.text()
        self.display_company_list.append(company_name)
        display_companylist(self)  # 显示到公司列表中
        self.extract_save_list[self.extract_list_index].addcompany(company_name)  # 添加到抽取保存列表中

    def extract_qlist_select(self, index):  # 选择抽取列表
        self.which_list_click = 0
        list_index = index.row()
        self.extract_list_index = list_index
        self.ui.cur_list_name.setText(self.extract_save_list[list_index].GetListName())  # 显示当前选中的抽取列表
        self.display_company_list = self.extract_save_list[list_index].GetCompanyList()
        display_companylist(self)

    def exit_function(self):
        self.hide()  # 隐藏此窗口
        self.father_ui.show()

    def save_setting_function(self):

        if self.ui.startNum_input.text().isdigit() == False:
            QMessageBox.information(self, "提示", "数字开始范围有误")
            return

        if self.ui.endNum_input.text().isdigit() == False:
            QMessageBox.information(self, "提示", "数字结束范围有误")
            return

        self.draw_number_list = list(range(int(self.ui.startNum_input.text()), int(self.ui.endNum_input.text())))
        if self.extract_save_list:
            self.save_extract.save_extral_list(self.extract_save_list)  # 保存所有抽取列表到josn文件中
        self.hide()  # 隐藏此窗口
        self.father_ui.show()

    def putback_index_change(self):
        self.no_put_back_flag = self.ui.putBackcomboBox.currentIndex()

    def company_list_clicked(self, qModelIndex):
        self.which_list_click = 1

    def GetCurrentCompanyList(self):
        ret = copy.deepcopy(self.display_company_list)
        return ret

    def GetCurrentNumList(self):
        ret = copy.deepcopy(self.draw_number_list)
        return ret

    def keyPressEvent(self, event):
        if (event.key() == Qt.Key_Delete):
            if 0 == self.which_list_click:
                for i in self.ui.extractList.selectedIndexes():
                    self.extract_save_list.pop(i.row())
                    self.display_extract_list.pop(i.row())
                display_extractlist(self)
                self.display_company_list = []
                display_companylist(self)
            else:
                for i in self.ui.companyList.selectedIndexes():
                    self.extract_save_list[self.extract_list_index].delcompany(i.data())
                    self.display_company_list.pop(i.row())
                display_companylist(self)

    def get_num_radiobtn_status(self):
        return self.ui.numradioButton.isChecked()

    def get_start_number(self):
        return self.ui.startNum_input.text()

    def get_end_number(self):
        return self.ui.endNum_input.text()
