# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'setttingDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_dialog(object):
    def setupUi(self, dialog):
        dialog.setObjectName("dialog")
        dialog.resize(681, 634)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("title_ico.jpg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        dialog.setWindowIcon(icon)
        self.putBackcomboBox = QtWidgets.QComboBox(dialog)
        self.putBackcomboBox.setGeometry(QtCore.QRect(20, 580, 101, 41))
        self.putBackcomboBox.setObjectName("putBackcomboBox")
        self.companyList = QtWidgets.QListView(dialog)
        self.companyList.setGeometry(QtCore.QRect(240, 180, 171, 361))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.companyList.setFont(font)
        self.companyList.setObjectName("companyList")
        self.AddCompany = QtWidgets.QPushButton(dialog)
        self.AddCompany.setGeometry(QtCore.QRect(480, 280, 121, 61))
        font = QtGui.QFont()
        font.setFamily("黑体")
        font.setPointSize(28)
        self.AddCompany.setFont(font)
        self.AddCompany.setObjectName("AddCompany")
        self.companyName = QtWidgets.QLineEdit(dialog)
        self.companyName.setGeometry(QtCore.QRect(480, 190, 121, 61))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.companyName.setFont(font)
        self.companyName.setObjectName("companyName")
        self.SaveButton = QtWidgets.QPushButton(dialog)
        self.SaveButton.setGeometry(QtCore.QRect(430, 580, 101, 41))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.SaveButton.setFont(font)
        self.SaveButton.setObjectName("SaveButton")
        self.BackButton = QtWidgets.QPushButton(dialog)
        self.BackButton.setGeometry(QtCore.QRect(560, 580, 101, 41))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.BackButton.setFont(font)
        self.BackButton.setObjectName("BackButton")
        self.line = QtWidgets.QFrame(dialog)
        self.line.setGeometry(QtCore.QRect(0, 80, 681, 20))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.line_2 = QtWidgets.QFrame(dialog)
        self.line_2.setGeometry(QtCore.QRect(0, 560, 681, 20))
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.AddDrawButton = QtWidgets.QPushButton(dialog)
        self.AddDrawButton.setGeometry(QtCore.QRect(10, 145, 31, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.AddDrawButton.setFont(font)
        self.AddDrawButton.setObjectName("AddDrawButton")
        self.extractList = QtWidgets.QListView(dialog)
        self.extractList.setGeometry(QtCore.QRect(10, 180, 171, 361))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.extractList.setFont(font)
        self.extractList.setObjectName("extractList")
        self.label_4 = QtWidgets.QLabel(dialog)
        self.label_4.setGeometry(QtCore.QRect(500, 140, 111, 41))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_4.setFont(font)
        self.label_4.setTextFormat(QtCore.Qt.PlainText)
        self.label_4.setObjectName("label_4")
        self.list_name = QtWidgets.QLineEdit(dialog)
        self.list_name.setGeometry(QtCore.QRect(50, 145, 121, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.list_name.setFont(font)
        self.list_name.setObjectName("list_name")
        self.cur_list_name = QtWidgets.QLabel(dialog)
        self.cur_list_name.setGeometry(QtCore.QRect(250, 150, 91, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.cur_list_name.setFont(font)
        self.cur_list_name.setText("")
        self.cur_list_name.setTextFormat(QtCore.Qt.PlainText)
        self.cur_list_name.setObjectName("cur_list_name")
        self.line_4 = QtWidgets.QFrame(dialog)
        self.line_4.setGeometry(QtCore.QRect(0, 20, 681, 20))
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.numradioButton = QtWidgets.QRadioButton(dialog)
        self.numradioButton.setGeometry(QtCore.QRect(10, 50, 115, 19))
        self.numradioButton.setObjectName("numradioButton")
        self.inputradioButton = QtWidgets.QRadioButton(dialog)
        self.inputradioButton.setGeometry(QtCore.QRect(10, 110, 115, 19))
        self.inputradioButton.setObjectName("inputradioButton")
        self.startNum_input = QtWidgets.QLineEdit(dialog)
        self.startNum_input.setGeometry(QtCore.QRect(160, 40, 101, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.startNum_input.setFont(font)
        self.startNum_input.setObjectName("startNum_input")
        self.endNum_input = QtWidgets.QLineEdit(dialog)
        self.endNum_input.setGeometry(QtCore.QRect(310, 40, 101, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.endNum_input.setFont(font)
        self.endNum_input.setObjectName("endNum_input")
        self.label = QtWidgets.QLabel(dialog)
        self.label.setGeometry(QtCore.QRect(270, 40, 31, 41))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.line_3 = QtWidgets.QFrame(dialog)
        self.line_3.setGeometry(QtCore.QRect(200, 90, 20, 481))
        self.line_3.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")

        self.retranslateUi(dialog)
        QtCore.QMetaObject.connectSlotsByName(dialog)

    def retranslateUi(self, dialog):
        _translate = QtCore.QCoreApplication.translate
        dialog.setWindowTitle(_translate("dialog", "Setting"))
        self.AddCompany.setText(_translate("dialog", "+"))
        self.SaveButton.setText(_translate("dialog", "保存设置"))
        self.BackButton.setText(_translate("dialog", "取消"))
        self.AddDrawButton.setText(_translate("dialog", "+"))
        self.label_4.setText(_translate("dialog", "输入公司名"))
        self.numradioButton.setText(_translate("dialog", "根据数字抽取"))
        self.inputradioButton.setText(_translate("dialog", "根据输入抽取"))
        self.label.setText(_translate("dialog", "----"))
