import json
import copy
from ExtractCompany import ExtractObject


class SaveExtraListInFile(object):
    def __init__(self, name):
        self.file_name = name

    def save_extral_list(self, extract_obj_list):
        extract_list_dict = dict()
        for obj in extract_obj_list:
            extract_list_dict[obj.GetListName()] = obj.GetCompanyList()
        try:
            with open(self.file_name, 'w') as f:
                json.dump(extract_list_dict, f)
        except Exception as e:
            print("File Error:" + str(e))  # str()将对象转换为字符串

    def get_extral_list(self):
        list = []
        try:
            with open(self.file_name, 'r') as f:
                dict_obj = json.load(f)
        except Exception as e:
            print("File Error:" + str(e))  # str()将对象转换为字符串
            return copy.deepcopy(list)

        for key in dict_obj:
            extract_obj = ExtractObject(key)
            extract_obj.SetCompanyList(dict_obj[key])
            list.append(extract_obj)
        return copy.deepcopy(list)





