
import xlwt
import time

class xlasWriterObj(object):
    def create_work_book(file_name,table_data):
        workbook = xlwt.Workbook(encoding='ascii')
        worksheet = workbook.add_sheet('My Worksheet')
        style = xlwt.XFStyle()  # 初始化样式
        font = xlwt.Font()  # 为样式创建字体
        font.name = 'Times New Roman'
        font.bold = True  # 黑体
        font.underline = True  # 下划线
        font.italic = True  # 斜体字
        style.font = font  # 设定样式

        worksheet.write(0, 0, "时间")
        worksheet.write(0, 1, "结果")
        for row in range(0, len(table_data) - 1):
            if None != table_data[row][0]:
                worksheet.write(row+1, 0, table_data[row][0])  # row, column, value
            if None != table_data[row][1]:
                worksheet.write(row+1, 1, table_data[row][1])

        workbook.save(file_name)  # 保存文件






