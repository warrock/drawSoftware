from draw import Ui_MainWindow
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QTimer, Qt
from Setting import ExtractSettingUi
from PyQt5.QtGui import QFont, QBrush
from xlasWriter import xlasWriterObj
import sys
import random
import time
import copy
import os

class main_window(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.extract.clicked.connect(self.extract_company)  # 抽奖按键回调extract_company方法
        self.ui.setting.triggered.connect(self.switch_settingwindow)  # 菜单选择设置回调switch_settingwindow方法
        self.timer1 = QTimer(self)  # 初始化一个定时器
        self.timer2 = QTimer(self)  # 初始化一个定时器
        self.timer1.timeout.connect(self.draw_result)  # 计时结束调用draw_result()方法，用户显示最终抽取结果
        self.timer2.timeout.connect(self.show_conpany_random)  # 计时结束调用show_conpany_random()方法，用于在界面随机显示公司名
        self.ui.clear_btn.clicked.connect(self.clear_table)
        self.ui.export_btn.clicked.connect(self.export_excel)
        self.son = ExtractSettingUi(self)

        self.initTableUi()
        self.company_list = []   # 用于抽取的公司列表
        self.number_list = []
        self.list_num = 0        # 公司列表长度
        self.current_company_index = 0  # 当前随机抽取的公司序号
        self.current_number = 0
        self.table_index = 0     # 当前表格显示的数目

    def draw_result(self):
        self.timer2.stop()   # 停止两个定时器
        self.timer1.stop()
        if 1 == self.son.no_put_back_flag:  # 若是不放回规则，则每次抽取后从列表取出公司名
            self.list_num -= 1
            if True == self.son.get_num_radiobtn_status():
                print(self.number_list)
                self.number_list.pop(self.current_company_index)
                print(self.number_list)
            else:
                self.company_list.pop(self.current_company_index)
                print(self.company_list)

        self.insert_new_row()

    def show_conpany_random(self):
        self.current_company_index = random.randint(0, self.list_num - 1)  # 生成随机数
        if True == self.son.get_num_radiobtn_status():
            current_number = self.number_list[self.current_company_index]
            self.ui.GetCompany.setText(str(current_number))
        else:
            current_company = self.company_list[self.current_company_index]    # 显示随机数
            self.ui.GetCompany.setText(current_company)
        self.timer2.start(80)  # 80毫秒显示一次

    def switch_settingwindow(self):
        self.hide()
        self.son.show()
        self.company_list = []  # 打开设置窗口则把当前公司列表清空
        self.number_list = []
        self.current_company_index = 0
        self.list_num = 0

    def extract_company(self):
        if True == self.son.get_num_radiobtn_status():
            if 0 == len(self.number_list):
                self.number_list = self.son.GetCurrentNumList()
                self.list_num = len(self.number_list)  # 求出列表长度
                if 0 == self.list_num:
                    QMessageBox.information(self, "提示", "没有数字范围")
                    return
        else:
            if 0 == len(self.company_list):
                self.company_list = self.son.GetCurrentCompanyList()  # 从设置中获取公司列表
                if not self.company_list:
                    QMessageBox.information(self, "提示", "没有输入公司名")
                    return
                self.list_num = len(self.company_list)  # 求出列表长度

        self.timer1.start(2000)  # 抽取过程为2秒
        self.timer2.start(80)    # 每80毫秒随机显示公司名

    def  initTableUi(self):
        horizontalHeader = ["时间" ,"结果" ,"备注"]
        self.ui.tableWidget.setColumnCount(3)  # 5列
        self.ui.tableWidget.setRowCount(0)
        # 隐藏垂直的表头，self.table.verticalHeader().setVisible(False)
        self.ui.tableWidget.setHorizontalHeaderLabels(horizontalHeader) # 水平表头，垂直表头setVerticalHeaderLabels()
        self.ui.tableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
        self.ui.tableWidget.setSelectionBehavior(QTableWidget.SelectColumns)
        self.ui.tableWidget.setSelectionMode(QTableWidget.SingleSelection  )

        # 表头也是由多个item构成的，所以通过循环操作对每一个item进行操作
        for index in range(self.ui.tableWidget.columnCount()):
            headItem = self.ui.tableWidget.horizontalHeaderItem(index)
            headItem.setFont(QFont("song", 12, QFont.Bold))
            headItem.setForeground(QBrush(Qt.gray))
            headItem.setTextAlignment(0x000 | Qt.AlignVCenter)

        self.ui.tableWidget.setColumnWidth(2 ,200) # 设置第3列宽度200
        self.ui.tableWidget.setRowHeight(0 ,40) # 设置第一行高度40
        row_count = self.ui.tableWidget.rowCount()
        self.ui.tableWidget.insertRow(row_count)
        mainLayout = QHBoxLayout()
        mainLayout.addWidget(self.ui.tableWidget)

    def insert_new_row(self):
        self.ui.tableWidget.setItem(self.table_index, 0, QTableWidgetItem(time.strftime("%H:%M:%S", time.localtime())))
        text = self.ui.GetCompany.text()
        self.ui.tableWidget.setItem(self.table_index, 1, QTableWidgetItem(text))
        self.table_index += 1
        row_count = self.ui.tableWidget.rowCount()
        self.ui.tableWidget.insertRow(row_count)

    def clear_table(self):
        self.ui.tableWidget.clear()
        self.initTableUi()
        self.table_index = 0

    def export_excel(self):

        row_data = []
        table_data = []
        row_count = self.ui.tableWidget.rowCount()
        if row_count < 2:
            return
        file_path = QFileDialog.getSaveFileName(self, "save file", os.getcwd(),
                                                "xls files (*.xls);;all files(*.*)")
        for row in range(0, row_count):
            if None != self.ui.tableWidget.item(row, 0):
                row_data.append(self.ui.tableWidget.item(row, 0).text())
            if None != self.ui.tableWidget.item(row, 1):
                row_data.append(self.ui.tableWidget.item(row, 1).text())
            if None != self.ui.tableWidget.item(row, 2):
                row_data.append(self.ui.tableWidget.item(row, 2).text())
            table_data.append(copy.deepcopy(row_data))
            row_data.clear()
        xlasWriterObj.create_work_book(file_path[0], table_data)


if __name__ == '__main__':
    try:
        with open('stylesheet.qss') as f:
            qss = f.read()
            app = QtWidgets.QApplication(sys.argv)
            app.setStyleSheet(qss)
    except IOError as err:
        print("File Error:" + str(err))  # str()将对象转换为字符串

    window = main_window()
    window.show()
    sys.exit(app.exec_())
